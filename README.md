# aims2-ipxe

This project can help you test PXE on VMs, but normally you should just use spare physical nodes you may be having on our `IT Linux Support - Test Physical` Openstack tenant

## BIOS approach

As of August 2019, OpenStack cells running Neutron at CERN do not currently support DHCP passthrough of the 'filename' and 'next-server' variables.

This repository exists to ensure it's always possible to boot a BIOS virtual machine that will work with AIMS2, regardless of the underlying network infrastructure.

The script `generate-ipxe-image.sh` will create an iPXE image file that contains the variables 'filename' and 'next-server' hardcoded inside the configuration.

This means that it is no longer a requirement to set the LanDB::Os to 'LINUX' (aims) or 'PXELINTEST' (aimstest) when booting an OpenStack VM when using this image.

This script has the following prerequisites:

```bash
yum -y install syslinux wget dosfstools
```

Image build example:

```bash
# ./generate-ipxe-image.sh --help
./generate-ipxe-image.sh {prod/test} {lgcy}
pass 'prod' or 'test' to build for aims or aimstest.
pass 'lgcy' to build for BIOS Legacy machines, otherwise it defaults to BIOS bootloaders. Check https://linuxops.web.cern.ch/aims2/aims2legacyworkaround/ for more info
# ./generate-ipxe-image.sh prod
4+0 records in
4+0 records out
4194304 bytes (4.2 MB) copied, 0.00231967 s, 1.8 GB/s
mkfs.fat 3.0.20 (12 Jun 2013)
--2019-08-01 15:01:13--  http://boot.ipxe.org/ipxe.iso
Resolving boot.ipxe.org (boot.ipxe.org)... 2001:ba8:0:1d4::6950:5845, 212.13.204.60
Connecting to boot.ipxe.org (boot.ipxe.org)|2001:ba8:0:1d4::6950:5845|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 716800 (700K) [application/octet-stream]
Saving to: ‘ipxe.iso’

100%[=========================================================================================================>] 1,048,576     2.84MB/s   in 0.4s   

2021-06-03 11:49:49 (2.84 MB/s) - ‘ipxe.iso’ saved [1048576/1048576]

mount: /dev/loop2 is write-protected, mounting read-only
# ls -lh iPXE-20190801-aimsprod-bios.img
-rw-r--r--. 1 djuarezg djuarezg 4.0M Aug  1 15:01 iPXE-20210603-aimsprod-bios.img
```

Once the image is generated, you can upload the image to OpenStack glance from aiadm:

$ `eval $(ai-rc "IT Linux Support - CI")`

$ `openstack image create --private --disk-format raw --file iPXE-20190801-aimsprod.img iPXE-20190801-aimsprod`

## UEFI approach

See [LOS-378](https://its.cern.ch/jira/browse/LOS-378) for further reference on what has been tested.

For UEFI, the alternative is to generate the same image but to upload it according to the [Cloud UEFI docs](https://clouddocs.web.cern.ch/advanced_topics/uefi_support.html#how-to-upload-an-uefi-compatible-image-to-the-cern-cloud). This image **will not boot** as of today, but we must specify an image for a VM.

**This means that it required to set the LanDB::Os to 'LINUX' (aims) or 'PXELINTEST' (aimstest) when booting an OpenStack VM when using this image.**

The generated image must be uploaded with specific properties:

$ `eval $(ai-rc "IT Linux Support - CI")`

$ `openstack image create --private --disk-format raw --file iPXE-20190801-aimsprod.img iPXE-20190801-aimsprod-uefi --property hw_firmware_type='uefi' --property hw_machine_type='q35' --property architecture='x86_64'`

The uploaded image will not boot, press ENTER to get into the boot manager menu. Select `Boot Manager` -> `UEFI PXEv4`. This will start the default AIMS2 menu for UEFI entries.
