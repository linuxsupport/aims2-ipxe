#!/bin/bash

function usage {
  echo "$0 {prod/test} {lgcy}"
  echo "pass 'prod' or 'test' to build for aims or aimstest."
  echo "pass 'lgcy' to build for BIOS Legacy machines, otherwise it defaults to BIOS bootloaders. Check https://linuxops.web.cern.ch/aims2/aims2legacyworkaround/ for more info"
  exit
}

if [ $EUID -ne 0 ]; then
  echo "this script should be run as root, exiting"
  exit 1
fi

if [ ! -f /bin/syslinux ]; then
  echo "syslinux missing, please 'yum -y install syslinux'"
  exit 1
fi
nextserver=$(host -t A aims.cern.ch | awk '{print $4}' | head -1)
filename="/aims/loader/bios/lpxelinux.0"
release="aimsprod"
bootmode="bios"
if [ $# -eq 1 ]; then
  if [ $1 == "test" ]; then
    nextserver=$(host -t A aimstest.cern.ch | awk '{print $4}' | head -1)
    release="aimstest"
  elif [ $1 != "prod" ]; then
    usage
  else
    usage
  fi
elif [ $# -eq 2 ]; then
  if [ $1 == "test" ]; then
    nextserver=$(host -t A aimstest.cern.ch | awk '{print $4}' | head -1)
    release="aimstest"
  elif [ $1 != "prod" ]; then
    usage
  else
    usage
  fi
  if [ $2 == "lgcy" ]; then
    filename="/aims/loader/lgcy/lpxelinux.0"
    bootmode="lgcy"
  else
    usage
  fi
elif [ $# -ne 0 ]; then
  usage
fi

pxeimage=iPXE-`date +%Y%m%d`-$release-$bootmode.img

ipxemount=`mktemp -d`
cernipxemount=`mktemp -d`
dd if=/dev/zero of=$pxeimage bs=1M count=4

mkdosfs $pxeimage
losetup /dev/loop99 $pxeimage
mount /dev/loop99 $cernipxemount
if [ `getenforce` == "Enforcing" ]; then
  setenforce 0
  syslinux --install /dev/loop99
  setenforce 1
else
  syslinux --install /dev/loop99
fi
wget http://boot.ipxe.org/ipxe.iso
mount -o loop ipxe.iso $ipxemount
cp $ipxemount/ipxe.lkrn $cernipxemount
cat > "${cernipxemount}/cern.ipxe" << EOF
#!ipxe
prompt --key 0x02 --timeout 2000 Press Ctrl-B for the iPXE command line... && shell ||
dhcp
# ---
# Comment the following block if you use LanDB::Os metadata to let DHCP + AIMS decide automatically
set net0/filename $filename
set net0/next-server $nextserver
show net0/filename
show net0/next-server
# ---
autoboot
EOF
cat > "${cernipxemount}/syslinux.cfg" <<EOF
DEFAULT ipxe
LABEL ipxe
KERNEL ipxe.lkrn
INITRD cern.ipxe
EOF
sync
umount $ipxemount
umount $cernipxemount
losetup -d /dev/loop99
rm -f ipxe.iso
